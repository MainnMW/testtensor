
from Pages.base_page import BasePage
from Resources.locators import ImagePageLocators
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class ImagePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
    def get_images(self):
        """ Return click at images tab """
        return self.click(ImagePageLocators.yandex_images)
    def get_top_image(self):
        """ Return click at first image in list """
        return self.click(ImagePageLocators.yandex_images_top_image)
    def get_top_image_title(self):
        """ Return text in first images grid """
        return self.get_attribute(ImagePageLocators.yandex_images_top_image,'data-grid-text')
    def get_text_field(self):
        """ Return text in search field """
        return self.get_attribute(ImagePageLocators.yandex_images_text_field,'value')
    def get_image_list(self):
        """ Return text in first images grid """
        return self.click(ImagePageLocators.yandex_images_image_list)
    def get_image_current_src(self):
        """Returns if image is open or not"""
        return self.get_attribute(ImagePageLocators.yandex_images_container,'src')
    def get_image_forward_button(self):
        """Returns forward button"""
        return WebDriverWait(self.driver,10).until(EC.presence_of_all_elements_located(ImagePageLocators.yandex_images_list))[-1].click()
    def get_image_back_button(self):
        """Returns back button"""
        return WebDriverWait(self.driver,10).until(EC.presence_of_all_elements_located(ImagePageLocators.yandex_images_list))[0].click()
    def exist_image_link(self):
        """Checks if link to images url is viable"""
        return self.element_exists(ImagePageLocators.yandex_images)
    def exist_image_list(self):
        """Checks if list of images is visible"""
        return self.element_exists(ImagePageLocators.yandex_images_image_list)
    def exist_image_current(self):
        """Checks if list of images is visible"""
        return self.element_exists(ImagePageLocators.yandex_images_container)
    def get_current_url(self):
        """ Return current url """
        return self.driver.current_url
