
from Pages.base_page import BasePage
from Resources.locators import HomePageLocators
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class HomePage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
    def get_suggest(self)->bool:
        """Returns if suggest panel is open"""
        return self.element_exists(HomePageLocators.yandex_suggest_field)
    def get_searches(self):
        """Return all search results"""
        return self.find_elements(HomePageLocators.yandex_search_results)
    def get_first_link_in_search(self) -> str:
        """Return first href from search"""
        return self.get_searches()[0].get_attribute('href')
    def enter_text(self, querry):
        """ Enters custom text into search field """
        return super().enter_text(HomePageLocators.yandex_search_field, querry)
    def press_enter(self):
        """Press enter on input field"""
        return super().enter_text(HomePageLocators.yandex_search_field, Keys.RETURN)
    def check_search_field(self):
        """ Returns if serach field exists """
        return self.element_exists(HomePageLocators.yandex_search_field)
