from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, TimeoutException
class BasePage():
    """The BasePage class holds all common functionality across the website.
    Also provides a nice wrapper when dealing with selenium functions that may
    not be easy to understand.
    """

    def __init__(self, driver):
        """ This function is called every time a new object of the base class is created"""
        self.driver = driver

    def click(self, by_locator,time=10):
        """ Performs click on web element whose locator is passed to it"""
        self.find_element(by_locator).click()

    def enter_text(self, by_locator, text):
        """ Performs text entry of the passed in text, in a web element whose locator is passed to it"""
        return self.find_element(by_locator).send_keys(text)
    def get_title(self) -> str:
        """Returns the title of the page"""
        return self.driver.title
    def find_element(self, locator,time=10):
        """Method for finding element suitable by locator."""
        return WebDriverWait(self.driver,time).until(EC.visibility_of_element_located(locator))
    def find_elements(self, locator,time=10):
        """Method for finding a list of elements suitable by locator. Explicit wait of time"""
        return WebDriverWait(self.driver,time).until(EC.visibility_of_all_elements_located(locator))
    def go_to_site(self):
        """Method to open base site"""
        return self.driver.get('http://www.yandex.com')
    def get_attribute(self, locator, attribute, time = 10):
        """Abstract method for getting named attribute"""
        return WebDriverWait(self.driver,time).until(EC.visibility_of_element_located(locator)).get_attribute(attribute)
    def element_exists(self, by_locator):
        """Abstrac method for checking element by locator"""
        try:
            return self.find_element(by_locator)
        except TimeoutException:
            return False
