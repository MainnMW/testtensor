from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
import sys
import os
import unittest
from selenium.common.exceptions import NoSuchElementException, TimeoutException
import pytest

base_dir = os.path.dirname(__file__) or '.'
sys.path.append("..")

from Pages.base_page import BasePage
from Pages.ya_home_page import HomePage
from Pages.ya_images_page import ImagePage
@pytest.fixture
def driver():
    return webdriver.Chrome(service=Service(ChromeDriverManager().install()))
class TestHomePage():
    def test_home_page(self,driver):
        home_page = HomePage(driver)
        home_page.go_to_site()
        assert home_page.check_search_field(), f"Не найдена полоса поиска"
        home_page.enter_text("Тензор")
        assert home_page.get_suggest(), f"Не найдена таблица с подсказками"
        home_page.press_enter()
        assert home_page.get_first_link_in_search() == "https://tensor.ru/", f"Неверный первый результат"
    def test_home_page_empty_search(self, driver):
        home_page = HomePage(driver)
        home_page.go_to_site()
        home_page.enter_text(" ")
        assert not home_page.get_suggest()
    def test_home_page_mischief(self,driver):
        home_page = HomePage(driver)
        home_page.go_to_site()
        assert home_page.check_search_field(), f"Не найдена полоса поиска"
        home_page.enter_text("Тензор математика")
        assert home_page.get_suggest(), f"Не найдена таблица с подсказками"
        home_page.press_enter()
        tmp = home_page.get_first_link_in_search()
        assert  tmp != "https://tensor.ru/", f"Неверный первый результат"
class TestImagePage():
    def test_image_page_positive(self,driver):
        image_page = ImagePage(driver)
        image_page.go_to_site()
        assert image_page.exist_image_link(), f"Меню картинок не найдено"
        image_page.get_images()
        assert 'https://yandex.ru/images/' in image_page.get_current_url(), f"неправильный url"
        grid_title = image_page.get_top_image_title()
        image_page.get_top_image()
        assert grid_title == image_page.get_text_field(), f"Несовпадение текста открытой картинки и названия в поле поиска"
        assert image_page.exist_image_list(), f"Нельзя открыть картинку"
        image_page.get_image_list()
        first_image_src = image_page.get_image_current_src()
        image_page.get_image_forward_button()
        second_image_src = image_page.get_image_current_src()
        assert first_image_src!=second_image_src, f"Картинка не изменилась"
        image_page.get_image_back_button()
        second_image_src = image_page.get_image_current_src()
        assert first_image_src==second_image_src, f"Картинки не совпадают"
    def test_image_page_negative(self,driver):
        image_page = ImagePage(driver)
        image_page.go_to_site()
        assert image_page.exist_image_link(), f"Меню картинок не найдено"
        image_page.get_images()
        assert 'https://yandex.ru/images/' in image_page.get_current_url(), f"неправильный url"
        grid_title = image_page.get_top_image_title()
        image_page.get_top_image()
        assert grid_title == image_page.get_text_field(), f"Несовпадение текста открытой картинки и названия в поле поиска"
        assert image_page.exist_image_list(), f"Нельзя открыть картинку"
        assert not image_page.exist_image_current()
