from selenium.webdriver.common.by import By

class HomePageLocators():
    yandex_search_field = (By.XPATH, "//input[@id = 'text']")
    yandex_suggest_field = (By.XPATH, "//ul[@class='mini-suggest__popup-content']/parent::*")
    yandex_search_button = (By.XPATH, "//div[@class ='search2__button']")
    yandex_search_results = (By.XPATH, '//main//li//a')

class ImagePageLocators():
    yandex_images = (By.XPATH, "//a[@id='tab-images']")
    yandex_images_top_image = (By.XPATH, "//div[@class='PopularRequestList-Item PopularRequestList-Item_pos_0']")
    yandex_images_text_field = (By.XPATH, "//input[@class ='input__control mini-suggest__input']")
    yandex_images_image_list = (By.XPATH, "//div[@role ='listitem']/*[1]")
    yandex_images_container = (By.XPATH, "//img[@class ='MMImage-Preview']")#//div[@role ='list']//*[1]
    yandex_images_list = (By.XPATH, "//i[@class ='CircleButton-Icon']/parent::*")
